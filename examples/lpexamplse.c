/***********************************************************************
**
**
** Copyright (C) 2013 LP-Research
** All rights reserved.
** Contact: LP-Research (info@lp-research.com)
***********************************************************************/

#include "lpexamples.h"

LpmsHandle_t me1Handle;

void LPMS_configExample(void)
{

    //Switch sensor to command mode
    LPMS_setCommunicationMode(1, GOTO_COMMAND_MODE);//Default id = 1
    lpDelay(10);

    //Set IMU id
    me1Handle.Id = 2;
    LPMS_setImuId(me1Handle.Id);

    //Read sensor system configuration
    me1Handle.Config = LPMS_getConfig(me1Handle.Id);

    //Set output data freaquency
    me1Handle.StreamFreq = LPMS_STREAM_FREQ_50HZ;
    LPMS_setStreamFreq(me1Handle.Id, me1Handle.StreamFreq);


    //Set output data type
    me1Handle.dataType = LPMS_QUAT_OUTPUT_ENABLED |LPMS_EULER_OUTPUT_ENABLED;
    LPMS_setTransmitData(me1Handle.Id, me1Handle.dataType);

    //Save parameter to flash
    LPMS_saveParameter(me1Handle.Id);

    //Switch sensor to data stream mode
    LPMS_setCommunicationMode(me1Handle.Id, GOTO_STREAM_MODE);

}