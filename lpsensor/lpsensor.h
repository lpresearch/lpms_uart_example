/***********************************************************************
**
**
** Copyright (C) 2013 LP-Research
** All rights reserved.
** Contact: LP-Research (info@lp-research.com)
***********************************************************************/

#ifndef __LPSENSOR_H
#define __LPSENSOR_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "stdint.h"
#include "stdlib.h"
#include "string.h"

#include "lpcommunication.h"

//LPMS Command list
#define COMMAND_START_ADDRESS   0
// Acknowledged and Not-acknowledged identifier
#define REPLY_ACK                           (COMMAND_START_ADDRESS + 0)
#define REPLY_NACK                          (COMMAND_START_ADDRESS + 1)
// Configuration and status
#define GET_CONFIG                          (COMMAND_START_ADDRESS + 4)
#define GET_STATUS                          (COMMAND_START_ADDRESS + 5)
// Mode switching
#define GOTO_COMMAND_MODE                   (COMMAND_START_ADDRESS + 6)
#define GOTO_STREAM_MODE                    (COMMAND_START_ADDRESS + 7)
// Data transmission
#define GET_SENSOR_DATA                     (COMMAND_START_ADDRESS + 9)
#define SET_TRANSMIT_DATA                   (COMMAND_START_ADDRESS + 10)
#define SET_STREAM_FREQ                     (COMMAND_START_ADDRESS + 11)

#define WRITE_REGISTERS                     (COMMAND_START_ADDRESS + 15)
#define RESTORE_FACTORY_VALUE               (COMMAND_START_ADDRESS + 16)

// Reference setting and offset reset
#define START_MAG_CALIBRA                    (COMMAND_START_ADDRESS + 17)
#define SET_ORIENTATION_OFFSET              (COMMAND_START_ADDRESS + 18)
#define RESET_ORIENTATION_OFFSET            (COMMAND_START_ADDRESS + 82)
// IMU ID setting
#define SET_IMU_ID                          (COMMAND_START_ADDRESS + 20)
#define GET_IMU_ID                          (COMMAND_START_ADDRESS + 21)
// Gyroscope settings
#define START_GYR_CALIBRA                   (COMMAND_START_ADDRESS + 22)
#define SET_GYR_RANGE                       (COMMAND_START_ADDRESS + 25)
#define GET_GYR_RANGE                       (COMMAND_START_ADDRESS + 26)
// Accelerometer settings
#define SET_ACC_RANGE                       (COMMAND_START_ADDRESS + 31)
#define GET_ACC_RANGE                       (COMMAND_START_ADDRESS + 32)
// Magnetometer settings
#define SET_MAG_RANGE                       (COMMAND_START_ADDRESS + 33)
#define GET_MAG_RANGE                       (COMMAND_START_ADDRESS + 34)
// Filter settings
#define SET_FILTER_MODE                     (COMMAND_START_ADDRESS + 41)
#define GET_FILTER_MODE                     (COMMAND_START_ADDRESS + 42)
// UART
#define SET_UART_BAUDRATE                   (COMMAND_START_ADDRESS + 84)
#define GET_UART_BAUDRATE                   (COMMAND_START_ADDRESS + 85)
// Software Sync
#define START_SYNC                          (COMMAND_START_ADDRESS + 96)
#define STOP_SYNC                           (COMMAND_START_ADDRESS + 97)


// Stream frequency data values
#define LPMS_STREAM_FREQ_5HZ                    (uint32_t)5
#define LPMS_STREAM_FREQ_10HZ                   (uint32_t)10
#define LPMS_STREAM_FREQ_25HZ                   (uint32_t)25
#define LPMS_STREAM_FREQ_50HZ                   (uint32_t)50
#define LPMS_STREAM_FREQ_100HZ                  (uint32_t)100
#define LPMS_STREAM_FREQ_200HZ                  (uint32_t)200
#define LPMS_STREAM_FREQ_400HZ                  (uint32_t)400

#define LPMS_UART_BAUDRATE_MASK                 0x000000ff
#define LPMS_UART_BAUDRATE_19200                0x00
#define LPMS_UART_BAUDRATE_38400                0x01
#define LPMS_UART_BAUDRATE_57600                0x02
#define LPMS_UART_BAUDRATE_115200               0x03
#define LPMS_UART_BAUDRATE_230400               0x04
#define LPMS_UART_BAUDRATE_256000               0x05
#define LPMS_UART_BAUDRATE_460800               0x06
#define LPMS_UART_BAUDRATE_921600               0x07


#define LPMS_LINACC_OUTPUT_ENABLED              (uint32_t)(0x00000001 << 21)
#define LPMS_QUAT_OUTPUT_ENABLED                (uint32_t)(0x00000001 << 18)
#define LPMS_EULER_OUTPUT_ENABLED               (uint32_t)(0x00000001 << 17)
#define LPMS_ANGULAR_VELOCITY_OUTPUT_ENABLED    (uint32_t)(0x00000001 << 16)
#define LPMS_TEMPERATURE_OUTPUT_ENABLED         (uint32_t)(0x00000001 << 13)
#define LPMS_GYR_RAW_OUTPUT_ENABLED             (uint32_t)(0x00000001 << 12)
#define LPMS_ACC_RAW_OUTPUT_ENABLED             (uint32_t)(0x00000001 << 11)
#define LPMS_MAG_RAW_OUTPUT_ENABLED             (uint32_t)(0x00000001 << 10)

#if defined(LPMS_CURS2)
#define LPMS_ALTITUDE_OUTPUT_ENABLED            (uint32_t)(0x00000001 << 19)
#define LPMS_HEAVEMOTION_OUTPUT_ENABLED         (uint32_t)(0x00000001 << 14)
#define LPMS_PRESSURE_OUTPUT_ENABLED            (uint32_t)(0x00000001 << 9)
#endif/*LPMS_CURS2*/

#define LPMS_DATA_MASK  (uint32_t)(0x17BF<<9)

#define LPMS_ALL_DATA_ENABLED   LPMS_LINACC_OUTPUT_ENABLED |LPMS_QUAT_OUTPUT_ENABLED |LPMS_EULER_OUTPUT_ENABLED \
                                                        | LPMS_ANGULAR_VELOCITY_OUTPUT_ENABLED | LPMS_TEMPERATURE_OUTPUT_ENABLED   \
                                                        |LPMS_GYR_RAW_OUTPUT_ENABLED |LPMS_ACC_RAW_OUTPUT_ENABLED | LPMS_MAG_RAW_OUTPUT_ENABLED
                                                        
#define     LPMS_CMD_PACKET_MAX_SIZE     15
#define MAX_PACKET_DATA_LENGTH 128

typedef struct _LpmsModbusPacket
{
    uint8_t start;
    uint16_t address;
    uint16_t function;
    uint16_t length;
    uint8_t data[MAX_PACKET_DATA_LENGTH];
    uint16_t lrcCheck;
    uint16_t end;
} LpmsPacket;

typedef struct
{
    float time;
    float gyr[3];
    float acc[3];
    float mag[3];
    float angular[3];
    float quat[4];
    float euler[3];
        float linAcc[3];
    float temp;
#if defined(LPMS_CURS2)
    float pressure;
    float altitude;
    float heaveMotion;
#endif/*LPMS_CURS2*/
} LpmsData_t;

typedef struct
{
    uint16_t Id;
    uint32_t Config;
    uint32_t dataType;
    uint32_t Status;
    uint32_t StreamFreq;
    uint32_t Com_Mode;
    void *pData;
} LpmsHandle_t;

typedef union
{
    uint8_t u8vals[4];
    uint32_t u32val;
    float fval;
} f2int_t,int2f_t;

#define R_TO_D    (57.295780f)//Radian to Degree

extern LpmsData_t me1Data;

uint8_t LPMS_Init(void);
uint8_t LPMS_setImuId(uint16_t id);
uint8_t LPMS_sendCmd(uint16_t id,uint16_t cmd,uint16_t length,uint32_t data);
uint8_t LPMS_setCommunicationMode(uint16_t id,uint32_t mode);
uint8_t LPMS_setStreamFreq(uint16_t id,uint32_t freq);
uint32_t LPMS_getConfig(uint16_t id);
uint8_t LPMS_setConfig(uint16_t id,uint32_t config);
uint8_t LPMS_setTransmitData(uint16_t id,uint32_t datas);

uint8_t LPMS_startGyrCalibration(uint16_t id);
uint8_t LPMS_startMagCalibration(uint16_t id);
uint8_t LPMS_saveParameter(uint16_t id);
uint8_t LPMS_resetToFactory(uint16_t id);


uint8_t LPMS_getPacket(void);
uint8_t LPMS_parsePacket(void);
uint8_t LPMS_parseSensorData(void);
uint8_t LPMS_getCmdTxBuffer(uint16_t id,uint16_t cmd,uint16_t length,uint32_t data,uint8_t *buffer,uint8_t *buffer_size);
float int2float(float * _f,uint8_t * _u8);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif/*__LPSENSOR_H*/
