/***********************************************************************
**
**
** Copyright (C) 2013 LP-Research
** All rights reserved.
** Contact: LP-Research (info@lp-research.com)
***********************************************************************/

/***********************************************************************
**
**  This file is platform independent
***********************************************************************/

#include "lpsensor.h"

LpmsPacket rxPacket;
LpmsData_t me1Data;
extern uint16_t rxBufferPtr ;

static LpmsHandle_t hme1;

uint8_t LPMS_Init(void)
{

    hme1.pData = (void *)&me1Data;

    hme1.Config = LPMS_getConfig(1);

    LPMS_startRecieve(rxBuffer);

    return 1;
}

uint8_t LPMS_setImuId(uint16_t id)
{

    LPMS_sendCmd(hme1.Id, SET_IMU_ID, 4, (uint32_t)id);
    hme1.Id = id;
    return 1;
}


/**
  *@brief: set communication mode
  *@param id: imu id
  *@param mode: GOTO_COMMAND_MODE or GOTO_STREAM_MODE
  *@ret:
  */
uint8_t LPMS_setCommunicationMode(uint16_t id,uint32_t mode)
{
    hme1.Com_Mode = mode;
    LPMS_sendCmd(id, mode,0 , 0);
    return 1;
}
uint8_t LPMS_setStreamFreq(uint16_t id,uint32_t freq)
{
    hme1.StreamFreq = freq;
    LPMS_sendCmd(id, SET_STREAM_FREQ, 4,freq);
    return 1;
}

uint32_t LPMS_getConfig(uint16_t id)
{
    LPMS_sendCmd(id, GET_CONFIG, 0,0);
    while(rxCompelete == 0);
    LPMS_getPacket();
    if(rxPacket.function == GET_CONFIG)
    {
        hme1.Config = *(uint32_t *)rxPacket.data;
        return hme1.Config;
    }
    else
        return 0;

}
uint8_t LPMS_setConfig(uint16_t id,uint32_t config)
{
    hme1.Config = config;
    hme1.dataType = config & (~LPMS_DATA_MASK);
    LPMS_sendCmd(id, SET_TRANSMIT_DATA, 4,config);
    return 1;

}
uint8_t LPMS_setTransmitData(uint16_t id,uint32_t datas)
{
    if(!hme1.Config)
        hme1.Config = LPMS_getConfig(id);
    hme1.Config &= ~LPMS_DATA_MASK;
    hme1.Config |= (datas & LPMS_DATA_MASK);
    LPMS_sendCmd(id, SET_TRANSMIT_DATA, 4,datas);
    return 1;

}

/**
  *@brief: Start gyroscope calibration
  *@note : when call this function,system must wait for calibration compelete about 10s
  *@ret:
  */
uint8_t LPMS_startGyrCalibration(uint16_t id)
{
    LPMS_sendCmd(id, START_GYR_CALIBRA, 0, 0);
    //wait about 10 seconds
    return 1;
}

/**
  *@brief: Start  magnetometer calibration
  *@note : when call this function,system must wait for calibration compelete about 10s
  *@ret:
  */
uint8_t LPMS_startMagCalibration(uint16_t id)
{
    LPMS_sendCmd(id, START_MAG_CALIBRA, 0, 0);
    //wait about 10 seconds
    return 1;
}
uint8_t LPMS_saveParameter(uint16_t id)
{
    LPMS_sendCmd(id, WRITE_REGISTERS, 0, 0);
    return 1;
}

uint8_t LPMS_resetToFactory(uint16_t id)
{

    LPMS_sendCmd(id, RESTORE_FACTORY_VALUE, 0, 0);
    return 1;

}


/**
  *@brief: send a command to imu
  *@param id: imu id
  *@param cmd: LPMS sensor command number
  *@param length: transmit data length, must be 0 or 4 in bytes
  *@param data: transmit data
  *@ret:
  */
uint8_t LPMS_sendCmd(uint16_t id,uint16_t cmd,uint16_t length,uint32_t data)
{
    uint8_t txBuffer[LPMS_CMD_PACKET_MAX_SIZE];
    uint8_t size = 0;

    LPMS_getCmdTxBuffer(id, cmd, length, data, txBuffer,&size);

    if(size != 0)
    {
        LPMS_startSend(txBuffer, size);
        return 1;
    }
    else
        return 0;
}

/**
  *@brief: get a transmit buffer that contain a command packet
  *@param id: imu id
  *@param cmd: LPMS sensor command number
  *@param length: transmit data length, must be 0(no data) or 4(one word) in bytes
  *@param data: transmit data
  *@param buffer: pointer to save transmit packet buffer
  *@param buffer_size: pointer to save transmit buffer size
  *@ret: 1
  */
uint8_t LPMS_getCmdTxBuffer(uint16_t id,uint16_t cmd,uint16_t length,uint32_t data,uint8_t *buffer,uint8_t *buffer_size)
{
    uint16_t check_sum = 0,i = 0;

    uint8_t *p;

    buffer[0] = 0x3A;
    buffer[1] = (id & 0xFF);
    buffer[2] = ((id >> 8) & 0xFF);
    buffer[3] = (cmd & 0xFF);
    buffer[4] = ((cmd >> 8) & 0xFF);
    buffer[5] = (length & 0xFF);
    buffer[6] = ((length >> 8) & 0xFF);

    if(length != 0)
    {
        p = (uint8_t *)&data;
        for(i = 0; i < 4; i++)
        {
            buffer[7+i] = p[i];
        }
    }

    for(i = 1; i < 7 + length; i++)
    {
        check_sum += buffer[i];
    }

    buffer[7 + length] = (check_sum & 0xFF);
    buffer[8 + length] = ((check_sum >> 8) & 0xFF);
    buffer[9 + length] = 0x0D;
    buffer[10 + length] = 0x0A;

    *buffer_size = 11 + length;

    return 1;
}
/**
  *@brief: get a lpms packet from recieved data buffer
  *@param:
  *@ret:
  */
uint8_t LPMS_getPacket(void)
{
    uint16_t parsePtr = 0;
    uint16_t check_sum = 0;
    uint16_t i = 0;
    uint8_t buffer[MAX_RX_BUFFER_SIZE] = {0};

    if(rxBuffer[rxBufferPtr] != 0x3A)
        return 0;

    parsePtr = rxBufferPtr;

    for(i = 0; i< rxSize; i++)
    {
        buffer[i] = rxBuffer[parsePtr++];
        if(parsePtr >= MAX_RX_BUFFER_SIZE)
            parsePtr %= MAX_RX_BUFFER_SIZE;
    }

    rxPacket.start = buffer[0];
    rxPacket.address = (uint16_t)((buffer[2] << 8) | buffer[1]);
    rxPacket.function = (uint16_t)((buffer[4] << 8) | buffer[3]);
    rxPacket.length = (uint16_t)((buffer[6] << 8) | buffer[5]);

    for(i = 0; i < rxPacket.length; i++)
    {
        rxPacket.data[i] = buffer[7 + i];
    }

    for(i = 1; i< 7+rxPacket.length; i++)
    {
        check_sum += buffer[i];
    }

    rxPacket.lrcCheck = (uint16_t)((buffer[8 + rxPacket.length] << 8) | buffer[7 + rxPacket.length]);

    if(check_sum != rxPacket.lrcCheck)
    {
        memset((uint8_t *)&rxPacket,0,sizeof(rxPacket));
        return 0;
    }

    rxPacket.end = (uint16_t)((buffer[10 + rxPacket.length] << 8) | buffer[9 + rxPacket.length]);;


    return 1;
}

uint8_t LPMS_parsePacket(void)
{
    if(!LPMS_getPacket())
        return 0;

    switch(rxPacket.function)
    {
    case GET_SENSOR_DATA:
        LPMS_parseSensorData();
        break;
    case GET_CONFIG:
        hme1.Config = *(uint32_t *)rxPacket.data;
        break;
    case GET_IMU_ID:
        hme1.Id= *(uint32_t *)rxPacket.data;
        break;

    case REPLY_ACK:
        break;
    case REPLY_NACK:
        break;
    default:
        break;
    }
    rxCompelete = 0;
    return 1;
}

uint8_t LPMS_parseSensorData(void)
{
    uint16_t len = 0;
    uint8_t i;

    me1Data.time =(* (uint32_t *)&rxPacket.data[len]) * 0.0025f;

    len += 4;

    if(hme1.dataType & LPMS_GYR_RAW_OUTPUT_ENABLED)
    {
        for(i = 0; i<3; i++)
        {
            int2float(&me1Data.gyr[i],  (uint8_t *)&rxPacket.data[len]);
            len += 4;
        }
    }

    if(hme1.dataType & LPMS_ACC_RAW_OUTPUT_ENABLED)
    {
        for(i = 0; i<3; i++)
        {
            int2float(&me1Data.acc[i],  (uint8_t *)&rxPacket.data[len]);
            len += 4;
        }
    }

    if(hme1.dataType & LPMS_MAG_RAW_OUTPUT_ENABLED)
    {
        for(i = 0; i<3; i++)
        {
            int2float(&me1Data.mag[i],  (uint8_t *)&rxPacket.data[len]);
            len += 4;
        }
    }

    if(hme1.dataType & LPMS_ANGULAR_VELOCITY_OUTPUT_ENABLED )
    {
        for(i = 0; i<3; i++)
        {
            int2float(&me1Data.angular[i],  (uint8_t *)&rxPacket.data[len]);
            len += 4;
        }
    }

    if(hme1.dataType & LPMS_QUAT_OUTPUT_ENABLED)
    {
        for(i = 0; i<4; i++)
        {
            int2float(&me1Data.quat[i],  (uint8_t *)&rxPacket.data[len]);
            len += 4;
        }
    }

    if(hme1.dataType & LPMS_EULER_OUTPUT_ENABLED)
    {
        for(i = 0; i<3; i++)
        {
            int2float(&me1Data.euler[i],  (uint8_t *)&rxPacket.data[len]);
            me1Data.euler[i] *= R_TO_D;
            len += 4;
        }
    }

    if(hme1.dataType & LPMS_LINACC_OUTPUT_ENABLED)
    {
        for(i = 0; i<3; i++)
        {
            int2float(&me1Data.linAcc[i],  (uint8_t *)&rxPacket.data[len]);
            len += 4;
        }
    }

#if defined(LPMS_CURS2)
    if(hme1.dataType & LPMS_PRESSURE_OUTPUT_ENABLED)
    {
        int2float(&me1Data.pressure,  (uint8_t *)&rxPacket.data[len]);
        len += 4;
    }
#endif/*LPMS_CURS2*/

    if(hme1.dataType & LPMS_TEMPERATURE_OUTPUT_ENABLED)
    {
        int2float(&me1Data.temp,  (uint8_t *)&rxPacket.data[len]);
        len += 4;
    }

#if defined(LPMS_CURS2)
    if(hme1.dataType & LPMS_ALTITUDE_OUTPUT_ENABLED)
    {
        int2float(&me1Data.altitude,  (uint8_t *)&rxPacket.data[len]);
        len += 4;
    }

    if(hme1.dataType & LPMS_HEAVEMOTION_OUTPUT_ENABLED)
    {
        int2float(&me1Data.heaveMotion,  (uint8_t *)&rxPacket.data[len]);
        len += 4;
    }
#endif/*LPMS_CURS2*/

    if(len == rxPacket.length)
        return 1;
    else
        return 0;
}

float int2float(float * p_f,uint8_t *p_u8)
{
    int2f_t i2f;
    for(uint8_t i=0; i < 4; i++)
        i2f.u8vals[i] = p_u8[i];
    *p_f = i2f.fval;
    return i2f.fval;
}
