/***********************************************************************
**
**
** Copyright (C) 2013 LP-Research
** All rights reserved.
** Contact: LP-Research (info@lp-research.com)
***********************************************************************/

#ifndef __LPCOMMUNICATION_H
#define __LPCOMMUNICATION_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "stdint.h"

#include "usart.h"
#include "dma.h"


#define USE_LPMS_UART

#define MAX_RX_BUFFER_SIZE  128

extern uint8_t rxBuffer[MAX_RX_BUFFER_SIZE];
extern uint16_t rxBufferPtr ;
extern uint8_t rxCompelete;
extern uint16_t rxSize;

uint8_t LPMS_startSend(uint8_t *pData,uint16_t size);
uint8_t LPMS_startRecieve(uint8_t *pData);
void lpDelay(uint32_t nms);


void HAL_UART_IdleCallback(UART_HandleTypeDef *huart);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif/*__LPCOMMUNICATION_H*/
