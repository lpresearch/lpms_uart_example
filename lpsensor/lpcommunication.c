/***********************************************************************
**
**
** Copyright (C) 2013 LP-Research
** All rights reserved.
** Contact: LP-Research (info@lp-research.com)
***********************************************************************/

/***********************************************************************
**
**  This file depends on platform
***********************************************************************/

#include "lpcommunication.h"

uint8_t rxBuffer[MAX_RX_BUFFER_SIZE];
uint16_t rxBufferPtr = 0;
uint8_t rxCompelete = 0;
uint16_t rxSize = 0;

#ifdef USE_LPMS_UART
uint8_t LPMS_startSend(uint8_t *pData,uint16_t size)
{
    HAL_UART_Transmit(&huart1, pData, size, 0xFFFF);
    // HAL_UART_Transmit_DMA(&huart1, pData, size);
    return 1;
}
uint8_t LPMS_startRecieve(uint8_t *pData)
{
    HAL_UART_Receive_DMA(&huart1, pData, MAX_RX_BUFFER_SIZE);

    return 1;
}

void lpDelay(uint32_t nms)
{
    HAL_Delay(nms);

}

/*
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{

   LPMS_startRecieve(rxBuffer);

}
*/

void HAL_UART_IdleCallback(UART_HandleTypeDef *huart)
{
    if(__HAL_UART_GET_IT_SOURCE(&huart1, UART_IT_IDLE) != RESET)
    {
        __HAL_UART_CLEAR_IDLEFLAG(&huart1);

        uint16_t rxConter = MAX_RX_BUFFER_SIZE - __HAL_DMA_GET_COUNTER(huart1.hdmarx);
        rxBufferPtr += rxSize;

        if(rxBufferPtr >= MAX_RX_BUFFER_SIZE)
        {
            rxBufferPtr %= MAX_RX_BUFFER_SIZE ;
        }

        if(rxBufferPtr < rxConter)
            rxSize = rxConter - rxBufferPtr  ;
        else
            rxSize = rxConter + MAX_RX_BUFFER_SIZE - rxBufferPtr;

        rxCompelete = 1;
    }

}
#endif /*USE_LPMS_UART*/
